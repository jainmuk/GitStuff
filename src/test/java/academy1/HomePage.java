package academy1;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.testng.annotations.Test;


import academy.Base;

public class HomePage extends Base {
	
	public org.apache.logging.log4j.Logger log = LogManager.getLogger(Base.class.getName()) ;
	
	@Test
	public void BasePageNavigation() throws IOException
	{
		driver = initializeDriver();
		driver.get("https://www.gmail.com");
		log.info("gmail launched");
		
		
	}
}
