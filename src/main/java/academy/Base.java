package academy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Base {
	
	public WebDriver driver;
	
	public WebDriver initializeDriver() throws IOException
	{
		Properties prop= new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\Dell\\workspace\\Maven\\src\\main\\java\\academy\\data.properties");
		prop.load(fis);
		String browserName= prop.getProperty("Browser");
		
		if(browserName.equals("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dell\\workspace\\chromedriver.exe");
			 driver = new ChromeDriver();			
		}
		else if(browserName.equals("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\Dell\\workspace\\geckodriver.exe");
			 driver = new FirefoxDriver();		
		}
		else if(browserName.equals("InternetExplorer"))
		{
			System.setProperty("webdriver.ie.driver", "");
			 driver = new InternetExplorerDriver();		
		}
			
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}

}
